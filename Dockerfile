FROM nvidia/cuda:10.1-devel-ubuntu18.04
# FROM ubuntu:18.04

ARG MAKE_PROC=1
ARG CONFIGURE_FLAGS="--enable-shared"
ARG COMMIT="master"

# get all the system level dependencies
RUN apt-get update && apt-get -y install \
    g++ \
    gcc \
    make \
    build-essential \
    git \
    autotools-dev \
    autoconf \
    automake \
    autogen \
    libtool \
    libtool-bin \
    pkg-config \
    gfortran \
		libfftw3-3 \
		libfftw3-bin \
		libfftw3-dev \
 		libfftw3-single3 \
    hwloc \
    python \
    python-dev \
    csh

ENV PSRHOME=/home/psr
ENV PSRDADA_SOURCE=$PSRHOME/sources/psrdada
ENV PSRDADA_BUILD=$PSRHOME/build/psrdada
ENV PSRDADA_INSTALL=$PSRHOME/software/psrdada

ENV CUDA_DIR=/usr/local/cuda
ENV CUDA_INCLUDE_DIR=$CUDA_DIR/include
ENV CUDA_LIB_DIR=$CUDA_DIR/lib64

RUN git clone https://git.code.sf.net/p/psrdada/code $PSRDADA_SOURCE
# COPY psrdada $PSRDADA

WORKDIR $PSRDADA_SOURCE
RUN git checkout $COMMIT && git pull
RUN ./bootstrap

WORKDIR $PSRDADA_BUILD
RUN $PSRDADA_SOURCE/configure $CONFIGURE_FLAGS --prefix=$PSRDADA_INSTALL \
  --with-cuda-include-dir=$CUDA_INCLUDE_DIR --with-cuda-lib-dir=$CUDA_LIB_DIR
RUN make -j $MAKE_PROC
RUN make install

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PSRDADA_INSTALL/lib
ENV CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$PSRDADA_INSTALL/include
ENV PATH=$PATH:$PSRDADA_INSTALL/bin

CMD dada_edit -h
