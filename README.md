Dockerfile for [psrdada](https://sourceforge.net/p/psrdada/wiki/Home/)

As of version 0.3.0, this Docker image builds on top of CUDA.

## Build

```
docker build -t psrdada --build-arg MAKE_PROC=8 .
```

## Run

```
docker run -it psrdada
```
