USER=dshaff
PROJECT=psrdada
PROC_ARGS := --build-arg MAKE_PROC=$(shell nproc)
VERSION := $(shell cat VERSION)
COMMIT=master
DOCKER_ARGS=

build:
	docker build $(DOCKER_ARGS) $(PROC_ARGS) --build-arg COMMIT=$(COMMIT) -t $(USER)/$(PROJECT):latest .

tag:
	docker tag $(USER)/$(PROJECT):latest $(USER)/$(PROJECT):$(VERSION)

all: build tag
